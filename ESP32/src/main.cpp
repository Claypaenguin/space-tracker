#include <Arduino.h>
#include <AccelStepper.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include "TickTwo.h"
#include <Sgp4.h>

// Global variables for observer's location and TLE data
double observerLat = 0.0;
double observerLng = 0.0;
double observerAlt = 0.0;
char satname[32];   // Ensure this is large enough to hold the satellite name
char tle_line1[70]; // Standard TLE line length
char tle_line2[70]; // Standard TLE line length

// Global flag to indicate that new data has been received
bool newDataReceived = false;

// Global flag to indicate that motors have been homed
bool stepperAz_homed = false;
bool stepperAlt_homed = false;

// Motor configuration
const int gearRatio = 5;
const int stepsPerRevolutionAz = 2048;
const int stepsPerRevolutionAlt = 2320;
const int totalSteps = gearRatio * stepsPerRevolutionAz;

// BLE configuration
BLEServer *pServer = NULL;
BLECharacteristic *pCombinedCharacteristic = NULL;
BLECharacteristic *pSatelliteCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;

// SGP4
Sgp4 sat;
unsigned long unixtime;

// Sensor pin definition
const int hall_sensor_pin1 = D8;
const int hall_sensor_pin2 = D9;
const int led_pin = D10;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/
#define SERVICE_UUID "62f7be24-e415-4c1a-82d4-1ded72026831"
#define COMBINED_CHARACTERISTIC_UUID "b3447ef6-076f-4500-bf45-0212800cab84"
#define SATELLITE_CHARACTERISTIC_UUID "3cbea1e3-879b-4e47-b115-d44700de5db8"

// Motor Driver Pins
#define IN1_2 D7
#define IN2_2 D6
#define IN3_2 D5
#define IN4_2 D4
#define IN1 D3
#define IN2 D2
#define IN3 D1
#define IN4 D0

// initialize the stepper library
AccelStepper stepperAz(AccelStepper::FULL4WIRE, IN1, IN3, IN2, IN4);
AccelStepper stepperAlt(AccelStepper::FULL4WIRE, IN1_2, IN2_2, IN3_2, IN4_2);

void toggleLED()
{
  digitalWrite(led_pin, !digitalRead(led_pin));
}

// Blink LED every 0.1 s
TickTwo timerLed(toggleLED, 100, 0);

void home(int pin, AccelStepper &stepper, bool &homingStatus, int offset, int totalSteps)
{
  Serial.println("Starting homing process...");

  if (deviceConnected)
  {
    pCombinedCharacteristic->setValue("Homing");
    pCombinedCharacteristic->notify();
  }

  // Check the initial state of the sensor
  bool sensorState = digitalRead(pin);
  Serial.print("Initial sensor state: ");
  Serial.println(sensorState ? "HIGH" : "LOW");

  // Start blinking the LED during the homing process
  timerLed.start();

  // Move the stepper motor until the sensor state changes to LOW
  if (sensorState == HIGH)
  {
    Serial.println("Moving to find LOW...");
    stepper.move(totalSteps);
    while (digitalRead(pin) == HIGH)
    { // Wait until we find LOW state
      stepper.run();
      timerLed.update();
    }
    Serial.println("LOW state found.");
  }

  // After finding LOW, continue to move until we find HIGH again
  stepper.move(totalSteps);
  Serial.println("Moving to find HIGH...");
  while (digitalRead(pin) == LOW)
  { // Now wait until it reads HIGH again
    stepper.run();
    timerLed.update();
  }

  // Homing process is complete once HIGH state is found again
  Serial.println("Homing complete. Sensor state is HIGH.");
  homingStatus = true;

  if (offset != 0)
  {
    stepper.move(offset);
    while (stepper.distanceToGo() != 0)
    {
      stepper.run();
      timerLed.update();
    }
  }

  timerLed.stop();
  digitalWrite(led_pin, LOW); // Ensure the LED is turned off after stopping

  stepper.setCurrentPosition(0);
}

long azimuthToSteps(float azimuth)
{
  return long(azimuth / 360.0 * totalSteps);
}

long altitudeToSteps(float altitude)
{
  // Adjust the altitude range from [-90, 90] to [0, 180] first
  float adjustedAltitude = altitude + 90; // Now ranges from 0 to 180

  // Then map this adjusted altitude range to the stepper's step range for 180°
  return long(adjustedAltitude / 180.0 * (stepsPerRevolutionAlt / 2));
}

// Implement rollover
void moveToShortestPath(AccelStepper &stepper, long targetSteps, long axisTotalSteps)
{
  long currentSteps = stepper.currentPosition();
  long stepsToTarget = targetSteps - currentSteps;
  long stepsToTargetAbs = abs(stepsToTarget);

  // Determine if it's shorter to move forward or backward to the target position.
  // If the absolute step difference is less than or equal to half the steps of a full circle,
  // then moving forward is shorter. Otherwise, moving backward is shorter.
  bool moveForward = stepsToTargetAbs <= (axisTotalSteps / 2);

  long shortestSteps;
  if (moveForward)
  {
    // If moving forward is shorter, then we use the direct step difference.
    shortestSteps = stepsToTarget;
  }
  else
  {
    // If moving backward is shorter, we need to determine whether to add or subtract
    // a "full circle" of steps to find the shortest path.
    if (stepsToTarget > 0)
    {
      // If the target is ahead (positive stepsToTarget), move backward by subtracting a "full circle".
      shortestSteps = stepsToTarget - axisTotalSteps;
    }
    else
    {
      // If the target is behind (negative stepsToTarget), move forward by adding a "full circle".
      shortestSteps = stepsToTarget + axisTotalSteps;
    }
  }
  stepper.move(shortestSteps);
}

void track_satellite()
{
  // Update the satellite position every three seconds
  sat.findsat(unixtime);
  unixtime += 3;

  // blink LED for every tracking interval
  digitalWrite(led_pin, !digitalRead(led_pin));

  // Convert azimuth/altitude to stepper motor steps
  long targetStepsAz = azimuthToSteps(sat.satAz);
  long targetStepsAlt = altitudeToSteps(sat.satEl);

  // Set target position
  moveToShortestPath(stepperAz, targetStepsAz, totalSteps);
  moveToShortestPath(stepperAlt, targetStepsAlt, (stepsPerRevolutionAlt / 2));

  Serial.print(String(sat.satName) + "Azimuth: " + String(sat.satAz) + "°" + " Altitude: " + String(sat.satEl) + "°" + " Distance: " + String(sat.satDist) + " km" + " Az Steps: " + String(targetStepsAz));

  // Notify data if device is connected
  if (deviceConnected)
  {
    String combinedData = String(sat.satAz) + "," + String(sat.satEl) + "," + String(sat.satDist);
    pCombinedCharacteristic->setValue(combinedData.c_str());
    pCombinedCharacteristic->notify();
    Serial.print(" (New data notified)");
  }
  Serial.println();
}

// Timer to call tracking function every 3 seconds
TickTwo timerSat(track_satellite, 3000, 0);

void start_advertising()
{
  pServer->startAdvertising(); // restart advertising
  Serial.println("Start advertising");
}

// Calls advertising function once after 500 ms to give bluetooth stack time to recover
TickTwo timerAdv(start_advertising, 500, 1);

class MyServerCallbacks : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    deviceConnected = true;
  };

  void onDisconnect(BLEServer *pServer)
  {
    deviceConnected = false;
  }
};

class MySatelliteCharacteristicCallbacks : public BLECharacteristicCallbacks
{
  void onWrite(BLECharacteristic *pSatelliteCharacteristic)
  {
    std::string receivedData = pSatelliteCharacteristic->getValue();
    if (receivedData.length() > 0)
    {
      Serial.println("Received data:");

      // Convert the std::string to a C string (null-terminated)
      const char *dataCString = receivedData.c_str();

      // Use strtok to split the data into lines
      const char *delimiter = "\n";
      char *line = strtok((char *)dataCString, delimiter);

      // We expect 6 lines of data: Unix Time, Latitude, Longitude, Satellite Name, TLE Line 1, TLE Line 2
      String dataLines[6];
      int lineIndex = 0;

      for (lineIndex = 0; line != NULL && lineIndex < 6; ++lineIndex)
      {
        dataLines[lineIndex] = line;    // Store the line in the corresponding position
        line = strtok(NULL, delimiter); // Get the next line
      }

      // Now dataLines array contains all the parsed lines
      if (lineIndex == 6)
      { // Check if we have all 6 lines
        Serial.println("Unix Time: " + dataLines[0]);
        Serial.println("Latitude: " + dataLines[1]);
        Serial.println("Longitude: " + dataLines[2]);
        Serial.println("Satellite Name: " + dataLines[3]);
        Serial.println("TLE Line 1: " + dataLines[4]);
        Serial.println("TLE Line 2: " + dataLines[5]);

        // Update Unix time
        unixtime = atol(dataLines[0].c_str());

        // Update observer's location
        observerLat = dataLines[1].toDouble();
        observerLng = dataLines[2].toDouble();

        // Update satellite name and TLE lines
        strncpy(satname, dataLines[3].c_str(), sizeof(satname));
        strncpy(tle_line1, dataLines[4].c_str(), sizeof(tle_line1));
        strncpy(tle_line2, dataLines[5].c_str(), sizeof(tle_line2));

        newDataReceived = true;
        Serial.println("Satellite tracking data updated.");
      }
      else
      {
        Serial.println("Incomplete data received.");
      }
    }
  }
};

void setup()
{
  Serial.begin(115200);

  // Set up pins
  pinMode(hall_sensor_pin1, INPUT);
  pinMode(hall_sensor_pin2, INPUT);
  pinMode(led_pin, OUTPUT);

  // Set the speed and acceleration
  stepperAz.setMaxSpeed(250);
  stepperAz.setSpeed(200);
  stepperAz.setAcceleration(100);
  stepperAlt.setMaxSpeed(250);
  stepperAlt.setSpeed(200);
  stepperAlt.setAcceleration(100);

  // Create the BLE Device
  BLEDevice::init("ESP32");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCombinedCharacteristic = pService->createCharacteristic(
      COMBINED_CHARACTERISTIC_UUID,
      BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE | BLECharacteristic::PROPERTY_NOTIFY | BLECharacteristic::PROPERTY_INDICATE);

  // Create Satellite Characteristic
  pSatelliteCharacteristic = pService->createCharacteristic(
      SATELLITE_CHARACTERISTIC_UUID,
      BLECharacteristic::PROPERTY_WRITE);

  // Register the callback for the Satellite Characteristic
  pSatelliteCharacteristic->setCallbacks(new MySatelliteCharacteristicCallbacks());

  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
  // Create BLE Descriptors
  pCombinedCharacteristic->addDescriptor(new BLE2902());
  pSatelliteCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0); // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting for a client connection to notify...");
}

void loop()
{
  // Update timers
  timerSat.update();
  timerAdv.update();

  if (newDataReceived)
  {
    if (timerSat.state())
    {
      timerSat.stop();
      // Make sure LED is turned off
      digitalWrite(led_pin, LOW);
    }

    if (!stepperAz_homed)
    {
      // We also need to turn an additional 90° for our azimuth home position
      home(hall_sensor_pin1, stepperAz, stepperAz_homed, 100 + (totalSteps / (360 / 90)), totalSteps);
    }

    if (!stepperAlt_homed)
    {
      home(hall_sensor_pin2, stepperAlt, stepperAlt_homed, 330, stepsPerRevolutionAlt);
    }

    newDataReceived = false; // Reset the flag
    sat.site(observerLat, observerLng, 0);
    sat.init(satname, tle_line1, tle_line2);
    timerSat.start();
    Serial.println("Satellite tracking started with new data.");
  }

  // If motors have been homed, run the stepper to track the satellite
  if (stepperAz_homed)
  {
    stepperAz.run(); // This will move the stepper motor according to the previously set target position
  }

  if (stepperAlt_homed)
  {
    stepperAlt.run();
  }

  // disconnecting
  if (!deviceConnected && oldDeviceConnected)
  {
    Serial.println("Device disconnected.");
    timerAdv.start(); // Delay advertising to give bluetooth stack some time
    oldDeviceConnected = deviceConnected;
  }

  // connecting
  if (deviceConnected && !oldDeviceConnected)
  {
    oldDeviceConnected = deviceConnected;
    Serial.println("Device Connected");
  }
}