// DOM Elements
const connectButton = document.getElementById('connectBleButton');
const disconnectButton = document.getElementById('disconnectBleButton');
const bleStateContainer = document.getElementById('bleState');
const timestampContainer = document.getElementById('timestamp');

const trackButton = document.getElementById('trackButton');
const satelliteInput = document.getElementById('satelliteInput');
const satelliteSentContainer = document.getElementById('satelliteSent');

const retrievedAzimuth = document.getElementById('valueContainerAzimuth');
const retrievedAltitude = document.getElementById('valueContainerAltitude');
const retrievedDistance = document.getElementById('valueContainerDistance');
const retrievedtrackingStatus = document.getElementById('valueContainerTrackingStatus');

//Define BLE Device Specs
var deviceName = 'ESP32';
var bleService = '62f7be24-e415-4c1a-82d4-1ded72026831';
var combinedCharacteristic = 'b3447ef6-076f-4500-bf45-0212800cab84';
var satelliteCharacteristic = '3cbea1e3-879b-4e47-b115-d44700de5db8';

//Global Variables to Handle Bluetooth
var bleServer;
var bleServiceFound;
var combinedCharacteristicFound;

// Connect Button (search for BLE Devices only if BLE is available)
connectButton.addEventListener('click', (event) => {
  if (isWebBluetoothEnabled()) {
    connectToDevice();
  }
});

// Disconnect Button
disconnectButton.addEventListener('click', disconnectDevice);

trackButton.addEventListener('click', () => writeSatelliteDataToCharacteristic(parseInt(satelliteInput.value)));

// Check if BLE is available in your Browser
function isWebBluetoothEnabled() {
  if (!navigator.bluetooth) {
    console.log('Web Bluetooth API is not available in this browser!');
    bleStateContainer.innerHTML = "Web Bluetooth API is not available in this browser/device!";
    return false
  }
  console.log('Web Bluetooth API supported in this browser.');
  return true
}

// Connect to BLE Device and Enable Notifications
function connectToDevice() {
  console.log('Initializing Bluetooth...');
  navigator.bluetooth.requestDevice({
    filters: [{ name: deviceName }],
    optionalServices: [bleService]
  })
    .then(device => {
      console.log('Device Selected:', device.name);
      bleStateContainer.innerHTML = 'Connected to device ' + device.name;
      bleStateContainer.style.color = "#24af37";
      device.addEventListener('gattservicedisconnected', onDisconnected);
      return device.gatt.connect();
    })
    .then(gattServer => {
      bleServer = gattServer;
      console.log("Connected to GATT Server");
      return bleServer.getPrimaryService(bleService);
    })
    .then(service => {
      bleServiceFound = service;
      console.log("Service discovered:", service.uuid);
      return service.getCharacteristic(combinedCharacteristic); // Change to your combined characteristic UUID
    })
    .then(characteristic => {
      console.log("Combined Characteristic discovered:", characteristic.uuid);
      combinedCharacteristicFound = characteristic;
      characteristic.addEventListener('characteristicvaluechanged', handleCombinedCharacteristicChange);
      characteristic.startNotifications();
      console.log("Notifications Started.");
      return characteristic.readValue();
    })
    .catch(error => {
      console.log('Error: ', error);
    })
}

function onDisconnected(event) {
  console.log('Device Disconnected:', event.target.device.name);
  bleStateContainer.innerHTML = "Device disconnected";
  bleStateContainer.style.color = "#d13a30";

  connectToDevice();
}

function handleCombinedCharacteristicChange(event) {
  const value = event.target.value;
  const decodedValue = new TextDecoder().decode(value);
  console.log("Received combined value: ", decodedValue);

  if (decodedValue) {
    if (decodedValue === "Homing") {
      retrievedtrackingStatus.innerHTML = "Homing..."
    } else {
      const [azimuth, altitude, distance] = decodedValue.split(',');
      retrievedAzimuth.innerHTML = azimuth + "°";
      retrievedAltitude.innerHTML = altitude + "°";
      retrievedDistance.innerHTML = distance + " km";
      retrievedtrackingStatus.innerHTML = "Tracking..."
      timestampContainer.innerHTML = getDateTime();
    }
  }
}

function writeSatelliteDataToCharacteristic(catnr) {
  if (bleServer && bleServer.connected) {
    // Check if the Geolocation API is supported
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(
        function (position) {
          // Success callback
          const latitude = position.coords.latitude;
          const longitude = position.coords.longitude;
          const unixTime = Math.floor(Date.now() / 1000); // Get the current Unix time in seconds
          console.log(`User's Location: Latitude: ${latitude}, Longitude: ${longitude}`);
          console.log(`User's Current Unix Time: ${unixTime}`);

          // Fetch the satellite data
          fetch(`https://celestrak.org/NORAD/elements/gp.php?CATNR=${catnr}&FORMAT=TLE`)
            .then(response => response.text())
            .then(data => {
              const tleData = data;
              console.log("Received TLE data:", tleData);
              const lines = tleData.trim().split('\n');
              const satname = lines[0];
              satelliteSentContainer.innerHTML = satname;

              // Include the user's location and Unix time in the TLE data
              const combinedData = `${unixTime}\n${latitude}\n${longitude}\n\n${tleData}`;

              return bleServiceFound.getCharacteristic(satelliteCharacteristic).then(characteristic => {
                console.log("Found the Satellite characteristic: ", characteristic.uuid);

                // Convert the combined data string (TLE data + location + Unix time) to a byte array for transmission
                const encoder = new TextEncoder('utf-8');
                const buffer = encoder.encode(combinedData);
                return characteristic.writeValue(buffer);
              });
            })
            .then(() => {
              console.log("Combined data written to Satellite characteristic:", catnr);
            })
            .catch(error => {
              console.error("Error fetching or writing combined data to the Satellite characteristic: ", error);
            });
        },
        function (error) {
          // Error callback
          console.error("Error occurred while getting user's location: " + error.message);
        },
        {
          // Optional: Configuration object for the geolocation request
          enableHighAccuracy: false,
          timeout: 10000,
          maximumAge: 0
        }
      );
    } else {
      console.error("Geolocation is not supported by this browser.");
    }
  } else {
    console.error("Bluetooth is not connected. Cannot write to characteristic.");
    window.alert("Bluetooth is not connected. Cannot write to characteristic. \n Connect to BLE first!");
  }
}

function disconnectDevice() {
  console.log("Disconnect Device.");
  if (bleServer && bleServer.connected) {
    // Stop notifications and remove event listeners for the combined characteristic
    let promiseChain = Promise.resolve();
    if (combinedCharacteristicFound) {
      promiseChain = combinedCharacteristicFound.stopNotifications()
        .then(() => {
          combinedCharacteristicFound.removeEventListener('characteristicvaluechanged', handleCombinedCharacteristicChange);
          console.log("Combined notifications stopped and listener removed.");
        });
    }

    // Disconnect the device
    promiseChain
      .then(() => bleServer.disconnect())
      .then(() => {
        console.log("Device Disconnected");
        bleStateContainer.innerHTML = "Device Disconnected";
        bleStateContainer.style.color = "#d13a30";
      })
      .catch(error => {
        console.log("An error occurred during disconnection:", error);
      });
  } else {
    console.error("Bluetooth is not connected.");
    window.alert("Bluetooth is not connected.");
  }
}

function getDateTime() {
  var currentdate = new Date();
  var day = ("00" + currentdate.getDate()).slice(-2); // Convert day to string and slice
  var month = ("00" + (currentdate.getMonth() + 1)).slice(-2);
  var year = currentdate.getFullYear();
  var hours = ("00" + currentdate.getHours()).slice(-2);
  var minutes = ("00" + currentdate.getMinutes()).slice(-2);
  var seconds = ("00" + currentdate.getSeconds()).slice(-2);

  var datetime = day + "/" + month + "/" + year + " at " + hours + ":" + minutes + ":" + seconds;
  return datetime;
}

