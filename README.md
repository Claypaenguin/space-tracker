# Space Tracker

This is the final project for the elective course "Fundamentals of Digital Fabrication". You can find the full documentation [here](https://fodf.bernwald.org/).

## Useful resources

- [Communicating with Bluetooth devices over JavaScript](https://developer.chrome.com/docs/capabilities/bluetooth)
- [ESP32 Web Bluetooth (BLE): Getting Started Guide](https://randomnerdtutorials.com/esp32-web-bluetooth/)
